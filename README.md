# Solnet Behat test helpers

## Solnet team details

Tech lead: Darren Inwood (darren.inwood@solnet.co.nz, 021 555 6543)

## Overview of the project

Solnet uses PHPUnit to run automated unit and functional tests.

This package configures your environment to run tests on Solnet infrastructure.

## Requirements

* SilverStripe 4.x

## Installation

It is recommended to install the ''solnet/silverstripe-test-helpers'' metapackage, which
installs both the Behat and PHPUnit repositories.  This will also mean that if in future
more test helpers are written (eg. for JavaScript aspects) these will be installed and
used automatically.

## Git branching strategy

This project uses the Solnet Git Branching Strategy, which is Gitflow with Pull Requests
for all merges to the ''develop'' branch.

All development should be done in a feature branch named after the JIRA issue; JIRA issues
should have 'Create branch' buttons that will create the branch for you, but if not, the branch
should be named eg. ''feature/ABCD-1234''.

All commit messages should start with the JIRA ticket number eg. ''ABCD-1234 Added widget editing''.

Commit and push your feature branch, and use the Bitbucket web interface to create a Pull Request
to merge into the ''develop'' branch.

## Tests

This metapackage does not have any tests of its own.

## Deployment

Releases and hotfixes are created using gitflow branches merged into master branch.

To create a new "release" version containing new features, follow this process:

1. Create release branch off ''develop'' named ''release/X.Y.Z'' - use Semver to determine the next version number, typically Y would be incremented and Z set to 0
2. Merge release branch into master
3. Create version number tag ''X.Y.Z'' matching the release branch
4. Merge changes from release branch into develop branch (to pull in hotfixes)
5. Delete release branch (optional)

To create a new "hotfix" version containing bugfixes, follow this process:

1. Create hotfix branch off ''master'' named ''hotfix/X.Y.Z'' - use Semver to determine the next version number, typically Z would be incremented
2. Merge hotfix branch into master
3. Create version number tag ''X.Y.Z'' matching the hotfix branch
4. Merge changes from hotfix branch into develop branch
5. Delete hotfix branch (optional)
